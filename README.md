The following steps shows how to install a DKIM in HostingRaja server

#!/bin/sh


domain="$1"


if [ -n "$domain" ]; then

echo "Domain argument is not empty"


mkdir /etc/opendkim/keys/$domain

/usr/sbin/opendkim-genkey -D /etc/opendkim/keys/$domain/ -d $domain -s default

chown -R opendkim: /etc/opendkim/keys/$domain

mv /etc/opendkim/keys/$domain/default.private /etc/opendkim/keys/$domain/default

echo "default._domainkey.$domain $domain:default:/etc/opendkim/keys/$domain/default" >> /etc/opendkim/KeyTable

echo "*@$domain default._domainkey.$domain" >> /etc/opendkim/SigningTable

chmod 0775 /etc/opendkim/keys/


chmod 0755 /etc/opendkim/keys/$domain


chmod 0755 /etc/opendkim/keys/$domain/default.txt


service opendkim restart


cat /etc/opendkim/keys/$domain/default.txt

echo "\n"

echo "Please add Above Dkim record to backend in DNS Management"

else

echo "Domain Argument is Empty"

fi


service postfix restart

service opendkim restart

service named restart


This above script for creating DKIM using opendkim. Before to use this script install opendkim to your server. Once you installed opendkim follow the script, It easily creates DKIM record.


Once DKIM record created, Add the DKIM record to your DNS Zone Editor.  In DNS Zone Editor, DKIM record put into as a target value. After doing everything restarts your postfix, opendkim and named service.


DKIM Uses:

1. Anti-phishing
2. Use with spam filtering
3. Compatibility
4. Protocol overhead
5. Non-repudiability


HostingRaja provides the simple front end interface to enter the DKIM values. To know more kindly navigate to HostingRaja.in 
